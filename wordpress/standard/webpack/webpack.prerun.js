const rimraf = require( 'rimraf' );

const packageJson = require( '../package.json' );
const themePath = 'wp-content/themes/' + packageJson.name;

rimraf( themePath + '/dist', {
	disableGlob: true,
}, () => console.log( 'webpack.prerun: Dist folder cleaned' ) );

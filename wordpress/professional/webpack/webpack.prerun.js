const rimrafSync = require( 'rimraf' ).rimrafSync;

const packageJson = require( '../package.json' );
const themePath = 'wp-content/themes/' + packageJson.name;

rimrafSync( themePath + '/dist', {
	disableGlob: true,
}, () => console.log( 'webpack.prerun: Dist folder cleaned' ) );

<?php

// ** URL settings ** //
define( 'WP_ROOT_URL', '{{ developmentUrl }}' );
define( 'WP_HOME', '{{ developmentUrl }}' );
define( 'WP_SITEURL', '{{ developmentUrl }}/wp' );

// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_HOST', '{{ databaseHostname }}' );
define( 'DB_NAME', '{{ databaseName }}' );
define( 'DB_USER', '{{ databaseUsername }}' );
define( 'DB_PASSWORD', '{{ databasePassword }}' );

// ** Enable development tools ** //
define( 'WP_DEBUG', '{{ debug }}' );

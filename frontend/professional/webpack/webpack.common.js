const Path = require( 'path' );
const { ProvidePlugin } = require( 'webpack' );
const ManifestPlugin = require( 'webpack-manifest-plugin' );
const FriendlyErrorsWebpackPlugin = require( 'friendly-errors-webpack-plugin' );

const packageJson = require( '../package.json' );
const baseDirectory = packageJson.config.baseDirectory;

module.exports = {
	entry: {
		app: Path.resolve( __dirname, '../assets/scripts/app.js' ),
	},
	output: {
		publicPath: baseDirectory + '/dist/',
		path: Path.resolve( __dirname, '../dist' ),
		filename: 'scripts/[name].js',
		chunkFilename: 'scripts/[name].chunk.js',
	},
	stats: false,
	externals: {
		jquery: 'jQuery',
	},
	plugins: [
		new ManifestPlugin(),
		new ProvidePlugin( {
			$: 'jquery',
			jQuery: 'jquery',
		} ),
		new FriendlyErrorsWebpackPlugin(),
	],
	resolve: {
		alias: {
			'assets': Path.resolve( __dirname, '../assets/' ),
			'components': Path.resolve( __dirname, '../components/' ),
		},
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				include: Path.resolve( __dirname, '../' ),
				enforce: 'pre',
				use: [
					{ loader: 'import-glob-loader' },
				],
			},
			{
				test: /\.js$/,
				include: Path.resolve( __dirname, '../' ),
				use: [
					{ loader: 'cache-loader' },
					{ loader: 'babel-loader' },
				],
			},
		],
	},
};

const rimraf = require( 'rimraf' );

rimraf( 'dist', {
	disableGlob: true,
}, () => {
	console.log( 'webpack.prerun: Dist folder cleaned' );
} );



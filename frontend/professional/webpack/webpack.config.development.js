const Path = require( 'path' );
const Webpack = require( 'webpack' );
const merge = require( 'webpack-merge' );
const BrowserSyncPlugin = require( 'browser-sync-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const ImageminWebpWebpackPlugin = require( 'imagemin-webp-webpack-plugin' );
const EsLintPlugin = require( 'eslint-webpack-plugin' );
const StylelintPlugin = require( 'stylelint-webpack-plugin' );

const common = require( './webpack.common.js' );
const packageJson = require( '../package.json' );
const config = packageJson.config;
const baseDirectory = packageJson.config.baseDirectory;

module.exports = merge( common, {
	mode: 'development',
	devtool: 'cheap-eval-source-map',
	devServer: {
		inline: true,
		hot: true,
	},
	plugins: [
		new Webpack.DefinePlugin( {
			'process.env.NODE_ENV': JSON.stringify( 'development' ),
		} ),
		new EsLintPlugin( {
			context: Path.resolve( __dirname, '../' ),
		} ),
		new StylelintPlugin( {
			context: Path.resolve( __dirname, '../' ),
			syntax: 'scss',
		} ),
		new CopyWebpackPlugin( [
			{
				from: Path.resolve( __dirname, '../assets/images' ),
				to: Path.resolve( __dirname, '../dist/images' ),
			},
			{
				from: Path.resolve( __dirname, '../assets/fonts' ),
				to: Path.resolve( __dirname, '../dist/fonts' ),
			},
		] ),
		new ImageminWebpWebpackPlugin( {
			config: [
				{
					test: /\.(jpe?g|png)/,
					include: Path.resolve( __dirname, '../' ),
					options: {
						quality: 75,
					},
				},
			],
		} ),
		new BrowserSyncPlugin( {
				proxy: config.developmentUrl,
				files: [
					'dist/**',
					'**/*.php',
					'**/*.html',
				],
				notify: {
					styles: {
						top: 'auto',
						bottom: 0,
						borderRadius: 0,
					},
				},
				logFileChanges: false,
			}
		),
	],
	module: {
		rules: [
			{
				test: /\.s?css$/i,
				include: Path.resolve( __dirname, '../' ),
				use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader?sourceMap=true' },
					{
						loader: 'sass-loader',
						options: {
							prependData: '$image-path: \'' + baseDirectory + '/dist/images/\';' +
							             '$fonts-path: \'' + baseDirectory + '/dist/fonts\';',
						},
					},
				],
			},
		],
	},
} );

const Path = require( 'path' );
const { ProvidePlugin } = require( 'webpack' );
const FriendlyErrorsWebpackPlugin = require( 'friendly-errors-webpack-plugin' );
const autoprefixer = require( 'autoprefixer' );

const packageJson = require( '../package.json' );
const baseDirectory = packageJson.config.baseDirectory;

module.exports = {
	entry: {
		app: Path.resolve( __dirname, '../assets/scripts/main.js' ),
		styles: Path.resolve( __dirname, '../assets/styles/main.css' ),
	},
	output: {
		publicPath: baseDirectory + '/dist/',
		path: Path.resolve( __dirname, '../dist' ),
	},
	stats: false,
	externals: {
		jquery: 'jQuery',
	},
	plugins: [
		new ProvidePlugin( {
			$: 'jquery',
			jQuery: 'jquery',
		} ),
		new FriendlyErrorsWebpackPlugin(),
	],
	module: {
		rules: [
			{
				test: /\.s?css$/i,
				include: Path.resolve( __dirname, '../' ),
				use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader?sourceMap=true' },
					{
						loader: 'postcss-loader',
						options: {
							plugins: [
								autoprefixer(),
							],
						},
					},
				],
			},
		],
	},
};

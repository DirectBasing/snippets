const Path = require( 'path' );
const Webpack = require( 'webpack' );
const merge = require( 'webpack-merge' );
const BrowserSyncPlugin = require( 'browser-sync-webpack-plugin' );
const EsLintPlugin = require( 'eslint-webpack-plugin' );
const StylelintPlugin = require( 'stylelint-webpack-plugin' );

const common = require( './webpack.common.js' );
const packageJson = require( '../package.json' );
const config = packageJson.config;

module.exports = merge( common, {
	mode: 'development',
	devtool: 'cheap-eval-source-map',
	devServer: {
		inline: true,
		hot: true,
	},
	plugins: [
		new Webpack.DefinePlugin( {
			'process.env.NODE_ENV': JSON.stringify( 'development' ),
		} ),
		new EsLintPlugin( {
			context: Path.resolve( __dirname, '../' ),
			files: 'assets/scripts/main.js',
		} ),
		new StylelintPlugin( {
			context: Path.resolve( __dirname, '../' ),
			files: 'assets/styles/main.css',
		} ),
		new BrowserSyncPlugin( {
				proxy: config.developmentUrl,
				files: [
					'dist/**',
					'**/*.php',
					'**/*.html',
				],
				notify: {
					styles: {
						top: 'auto',
						bottom: 0,
						borderRadius: 0,
					},
				},
				logFileChanges: false,
			},
		),
	],
} );
